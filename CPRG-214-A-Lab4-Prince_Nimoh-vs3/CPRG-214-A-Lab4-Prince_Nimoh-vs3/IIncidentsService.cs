﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace CPRG_214_A_Lab4_Prince_Nimoh_vs3
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IIncidentsService" in both code and config file together.
    [ServiceContract]
    public interface IIncidentsService
    {
        [OperationContract]
        List<Incident> GetOpenTechIncidents(int techID);

        [OperationContract]
        List<Incident> GetCustomerIncidents(int customerID);

        [OperationContract]
        List<Customer> GetAllCustomers();

    }

    [DataContract]
    public class Incident
    {
        [DataMember]
        public int IncidentID { get; set; } //Incident ID - primary key for incident table
        
        [DataMember]
        public string ProductCode { get; set; } //The code for the product the incident is related to - foriegn key

        [DataMember]
        public int? TechID { get; set; } //The ID for the technician assigned to the incident

        [DataMember]
        public DateTime DateOpened //The date the incident was opened
        {
            get
            {
                return dateOpened.Date;
            }
            set
            {
                dateOpened = value;
            }
        } 
        private DateTime dateOpened;

        [DataMember]
        public DateTime? DateClosed { get; set; } //The date the incident was closed

        [DataMember]
        public string Title { get; set; } //The title of the incident

        [DataMember]
        public string Description { get; set; } //The description of the incident
    }

    [DataContract]
    public class Customer
    {
        [DataMember]
        public int CustomerID { get; set; } //Customer ID - primary key for the customer table

        [DataMember]
        public string CustomerName { get; set; } //The name of the customer
    }
}
