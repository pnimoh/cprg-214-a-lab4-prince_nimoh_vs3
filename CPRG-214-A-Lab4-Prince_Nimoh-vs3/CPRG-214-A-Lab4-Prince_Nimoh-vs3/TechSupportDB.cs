﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CPRG_214_A_Lab4_Prince_Nimoh_vs3
{
 /**
 * Author: Prince Nimoh
 * Context: CPRG214-Lab3-Prince Nimoh
 * Student #: 000792122
 * Date: July 2018
 *DB class for connecting to the database
 * */
    public class TechSupportDB
    {
        public static SqlConnection GetConnection()
        {
            string connString = ConfigurationManager.ConnectionStrings["TechSupportConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connString);
            return conn;
        }
    }
}