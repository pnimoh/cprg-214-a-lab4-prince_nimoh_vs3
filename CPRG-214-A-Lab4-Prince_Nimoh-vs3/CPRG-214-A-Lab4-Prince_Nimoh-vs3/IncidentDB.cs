﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CPRG_214_A_Lab4_Prince_Nimoh_vs3
{
    /**
    * Author: Prince Nimoh
    * Context: CPRG214-Lab4-Prince Nimoh
    * Student #: 000792122
    * Date: July 2018
    *Data access class for the reading from the incidents table of the techsupport database.
    * */
    
    public class IncidentDB
    {
       
        public static List<Incident> GetOpenIncidentsByTechID(int techID)
        {
            List<Incident> incidents = new List<Incident>(); //make an empty list
            Incident incident = null; //reference for reading
            // define connection
            SqlConnection connection = TechSupportDB.GetConnection();

            // define the select query command
            string selectQuery = "Select IncidentID, ProductCode, TechID, DateOpened, Title, [Description] " +
                                 " from Incidents " +
                                 " where TechID = @TechID AND DateClosed is null " +
                                 " Order by DateOpened";

            SqlCommand selectCommand = new SqlCommand(selectQuery, connection);
            selectCommand.Parameters.AddWithValue("@TechID", techID);
            try
            {
                // open the connection
                connection.Open();

                // execute the query
                SqlDataReader reader = selectCommand.ExecuteReader();// can be multiple records

                // process the results
                while (reader.Read()) // while there are Incidents
                {
                    incident = new Incident();
                    incident.IncidentID = Convert.ToInt32(reader["IncidentID"]);
                    incident.TechID = Convert.ToInt32(reader["TechID"]);
  
                    incident.ProductCode = reader["ProductCode"].ToString();
                    incident.DateOpened = Convert.ToDateTime(reader["DateOpened"]);
                    incident.Title = reader["Title"].ToString();
                    incident.Description = reader["Description"].ToString();

                    incidents.Add(incident);
                }
            }
            catch (Exception ex)
            {
                throw ex; // let the form handle it
            }
            finally
            {
                connection.Close(); // close connecto no matter what
            }

            return incidents;
        }


        public static List<Incident> GetCustomerIncidents(int customerID)
        {
            List<Incident> incidents = new List<Incident>(); //make an empty list
            Incident incident = null; //reference for reading
            // define connection
            SqlConnection connection = TechSupportDB.GetConnection();

            // define the select query command
            string selectQuery = "Select IncidentID, ProductCode, TechID, DateOpened, DateClosed, Title, [Description] " +
                                  " from Incidents " +
                                  " where CustomerID = @CustomerID " +
                                  " Order by DateOpened";

            SqlCommand selectCommand = new SqlCommand(selectQuery, connection);
            selectCommand.Parameters.AddWithValue("@CustomerID", customerID);
            try
            {
                // open the connection
                connection.Open();

                // execute the query
                SqlDataReader reader = selectCommand.ExecuteReader();// can be multiple records

                // process the results
                while (reader.Read()) // while there are Incidents
                {
                    incident = new Incident();
                    incident.IncidentID = Convert.ToInt32(reader["IncidentID"]);
                    incident.TechID = (reader["TechID"] == System.DBNull.Value) ? (int?) null :
                                                                Convert.ToInt32(reader["TechID"]);
                    
                    incident.ProductCode = reader["ProductCode"].ToString();
                    incident.DateOpened = Convert.ToDateTime(reader["DateOpened"]);
                    incident.DateClosed = (reader["DateClosed"] == System.DBNull.Value) ? (DateTime?)null :
                                                                                        Convert.ToDateTime(reader["DateClosed"]);
                    incident.Title = reader["Title"].ToString();
                    incident.Description = reader["Description"].ToString();

                    incidents.Add(incident);
                }
            }
            catch (Exception ex)
            {
                throw ex; // let the form handle it
            }
            finally
            {
                connection.Close(); // close connecto no matter what
            }

            return incidents;
        }
    }
}