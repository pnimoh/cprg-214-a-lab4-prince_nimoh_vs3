﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CPRG_214_A_Lab4_Prince_Nimoh_vs3
{
    /**
    * Author: Prince Nimoh
    * Context: CPRG214-Lab4-Prince Nimoh
    * Student #:
    * Date: July 2018
    *Data access class for the customers class. 
    * */
    public class CustomerDB
    {
        /// <summary>
        /// Reads customer objects from the TechSupport DB
        /// </summary>
        /// <returns>returns a list of customers</returns>
        
        public static List<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>(); // make an empty list
            Customer customer; // reference to new state object
            // create connection
            SqlConnection connection = TechSupportDB.GetConnection();

            // create select command
            string selectString = "SELECT CustomerID, Name from Customers " +
                                  "order by CustomerID";
            SqlCommand selectCommand = new SqlCommand(selectString, connection);
            try
            {
                // open connection
                connection.Open();
                // run the select command and process the results adding technicians to the list
                SqlDataReader reader = selectCommand.ExecuteReader();
                while (reader.Read())// process next row
                {
                    customer = new Customer();
                    customer.CustomerID = Convert.ToInt32(reader["CustomerID"]);
                    customer.CustomerName = reader["Name"].ToString();
                    
                    customers.Add(customer);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex; // throw it to the form to handle
            }
            finally
            {
                connection.Close();
            }
            return customers;
        }
    }
}