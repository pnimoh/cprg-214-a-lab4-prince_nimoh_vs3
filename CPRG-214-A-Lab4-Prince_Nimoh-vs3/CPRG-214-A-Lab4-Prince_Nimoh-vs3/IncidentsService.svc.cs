﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

/**
 * Author: Prince Nimoh
 * Context: CPRG214-Lab4-Prince Nimoh
 * Student #: 000792122
 * Date: July 2018
 *Class for the incident service
 * */
namespace CPRG_214_A_Lab4_Prince_Nimoh_vs3
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "IncidentsService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select IncidentsService.svc or IncidentsService.svc.cs at the Solution Explorer and start debugging.
    public class IncidentsService : IIncidentsService
    {
        public List<Customer> GetAllCustomers()
        {
            return CustomerDB.GetAllCustomers() ;
        }

        public List<Incident> GetCustomerIncidents(int customerID)
        {
            return IncidentDB.GetCustomerIncidents(customerID);
        }

        public List<Incident> GetOpenTechIncidents(int techID)
        {
            return IncidentDB.GetOpenIncidentsByTechID(techID);
        }
    }
}
