﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CustomerIncidents.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="CSS/main.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <div class="jumbotron text-center">
            <h1>Tech Support Dashboard</h1>
        </div>
        <form id="form1" runat="server">
        <div>
            Select a customer:<br />
            <asp:DropDownList ID="ddlCustomers" runat="server" OnSelectedIndexChanged="ddlCustomers_SelectedIndexChanged" AutoPostBack="True">
            </asp:DropDownList>
            <br />
            <br />
            Customer Incidents:<br />
            <asp:GridView ID="gvIncidents" runat="server" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="Title" HeaderText="Title" />
                    <asp:BoundField DataField="Description" HeaderText="Description" />
                    <asp:BoundField DataField="IncidentID" HeaderText="Incident ID" />
                    <asp:BoundField DataField="DateOpened" DataFormatString="{0:d}" HeaderText="Date Opened" />
                    <asp:BoundField DataField="DateClosed" DataFormatString="{0:d}" HeaderText="Date Closed" />
                    <asp:BoundField DataField="ProductCode" HeaderText="Product Code" />
                    <asp:BoundField DataField="TechID" HeaderText="Tech ID" />
                </Columns>
                <EmptyDataTemplate>
                    <asp:Label ID="Label1" runat="server" Text="No incidents for the above customer"></asp:Label>
                </EmptyDataTemplate>
            </asp:GridView>
        </div>
    </form>
    </div>
    
    <script src="Scripts/jquery-3.0.0.js"></script>
    <script src="Scripts/popper.js"></script>
    <script src="Scripts/bootstrap.js"></script>
</body>
</html>
