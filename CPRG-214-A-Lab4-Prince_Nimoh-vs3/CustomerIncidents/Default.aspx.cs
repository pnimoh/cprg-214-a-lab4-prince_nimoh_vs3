﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CustomerIncidents.CustomerIncidentsService; //reference for service

namespace CustomerIncidents
{
 /**
 * Author: Prince Nimoh
 * Context: CPRG214-Lab4-Prince Nimoh
 * Student #: 000792122
 * Date: July 2018
 *Default page
 * */
    public partial class Default : System.Web.UI.Page
    {
        //create proxy object for the WCF service
        IncidentsServiceClient proxy = new IncidentsServiceClient();
        Customer[] customers;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                customers = proxy.GetAllCustomers();
                Session["custs"] = customers;
                ddlCustomers.DataSource = customers;
                ddlCustomers.DataTextField = "CustomerID";
                ddlCustomers.DataValueField = "CustomerID";
                ddlCustomers.DataBind();
                ddlCustomers.SelectedIndex = 0;
                ddlCustomers_SelectedIndexChanged(null, EventArgs.Empty);
            }
            else //retrive from session
            {
                customers = (Customer[])Session["custs"];
            }
            
           
        }

        protected void ddlCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            Customer customer = customers[ddlCustomers.SelectedIndex]; //Get the selected customer
            List<Incident> incidents =  
                proxy.GetCustomerIncidents(customer.CustomerID).ToList<Incident>(); //Get a list of incidents
            
            gvIncidents.DataSource = incidents; // Get the incidents as the datasource of the grid view.
            gvIncidents.DataBind();
        }
    }
}